#!/usr/bin/env python
# -*- coding: utf-8 -*-
import rospy
from geometry_msgs.msg import Twist

def move_and_stop():
    rospy.init_node('my_robot_controller', anonymous=True)
    pub = rospy.Publisher('/jetauto_controller/cmd_vel', Twist, queue_size=10)
    rate = rospy.Rate(10)  # 以10Hz的频率发送命令

    # 定义移动指令
    move = Twist()
    move.linear.x = 0.1  # 向前移动
    move.angular.z = 0.0  # 不旋转

    # 定义停止指令
    stop = Twist()
    stop.linear.x = 0.0  # 停止
    stop.angular.z = 0.0  # 停止旋转

    # 计时开始
    start_time = rospy.Time.now()

    while not rospy.is_shutdown():
        # 检查是否已经移动了一秒钟
        current_time = rospy.Time.now()
        if (current_time - start_time).to_sec() < 1.0:
            # 如果还没有到一秒钟，继续移动
            pub.publish(move)
        else:
            # 一秒钟之后，发送停止命令并退出循环
            pub.publish(stop)
            rospy.loginfo("停止")
            break  # 退出循环

        rate.sleep()  # 根据前面设置的频率暂停

if __name__ == '__main__':
    try:
        move_and_stop()
    except rospy.ROSInterruptException:
        pass
